# MPM in Julia

This repository provides a sandbox for experimenting with simple MPM solvers in Julia.

## Code

The examples and supporting code are found in the `src` folder.

## Original Source

This repository is based upon the Julia code from **Programming the material point method in Julia**.

The paper can be found in the `paper` folder.

That paper used Julia 0.4.5 and this repository uses Julia 1.8

See the original repository [here](https://github.com/vinhphunguyen/MPM-Julia).

See the original rebuild of this code [here](https://github.com/jeremylt/mpm-julia).
