# ------------------------------------------------------------------------------
# Plotting helper
# ------------------------------------------------------------------------------

function plotgridwithmaterialpoints(grid::Grid, materialpoints::AbstractArray{MaterialPoint}, E::Float64, title::String)
    # get locations, points
    materialpoints_x = [materialpoint.x[1] for materialpoint in materialpoints]
    materialpoints_y = [materialpoint.x[2] for materialpoint in materialpoints]
    materialpoints_color = [computecolor(materialpoint, E) for materialpoint in materialpoints]

    # plot
    scatter(
         materialpoints_x,
         materialpoints_y,
         color = materialpoints_color,
          aspect_ratio = :equal,
          lims = [0.0, max(grid.length[1], grid.length[2])],
          title = title,
          label = "",
          xlabel = "x",
          ylabel = "y",
    )
end

# ------------------------------------------------------------------------------
